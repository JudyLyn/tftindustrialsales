import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import bg from './images/bg.png';

import './App.css';
import mobileImage from './images/slides/bg-mobile.png';
import desktopImage from './images/slides/first.png';

//components
import AppNavbar from './components/AppNavbar';
import ProductsOne from './components/ProductsOne';
import ProductsTwo from './components/ProductsTwo';
import ProductsThree from './components/ProductsThree';
import ProductsFour from './components/ProductsFour';
import ProductsFive from './components/ProductsFive';
import ProductsSix from './components/ProductsSix';
import ProductsSeven from './components/ProductsSeven';
import ProductsEight from './components/ProductsEight';
import ProductsNine from './components/ProductsNine';
import ProductsTen from './components/ProductsTen';

//pages
import Home from './pages/Home';
import AboutUsPage from './pages/AboutUsPage';
import ContactUsPage from './pages/ContactUsPage';

//bootstrap
import { Container } from 'react-bootstrap';



function App() {
  return (
    <Router>
      <div className="bg_image"
            style={{
              backgroundImage: `url(${bg})`,
              backgroundSize: "cover",
              height: "100vh",  
            }}>
          <div className="sticky-top">
            < AppNavbar />
          </div>

          <Switch>
            < Route exact path="/" component={Home} />
            < Route exact path="/aboutUs" component={AboutUsPage} />
            < Route exact path="/plate-heat-exchanger-plates-and-gaskets" component={ProductsOne} />
            < Route exact path="/gaskets-and-packings" component={ProductsTwo} />
            < Route exact path="/moulded-rubber-fabrication" component={ProductsThree} />
            < Route exact path="/engineering-plastic-fabrication" component={ProductsFour} />
            < Route exact path="/compression-springs" component={ProductsFive} />
            < Route exact path="/caster-wheels" component={ProductsSix} />
            < Route exact path="/insulation-materials" component={ProductsSeven} />
            < Route exact path="/industrial-adhesive-tapes" component={ProductsEight} />
            < Route exact path="/lab-equipment" component={ProductsNine} />
            < Route exact path="/industrial-thermometers-and-pressure-gauge" component={ProductsTen} />
            < Route exact path="/contactUs" component={ContactUsPage} />
          </Switch>   
      </div>
    </Router>
    );
  }

  export default App;
