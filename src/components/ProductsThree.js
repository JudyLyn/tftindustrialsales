import { Card, Button, Row, Col, Image, Container, fluid } from 'react-bootstrap';
import four from '../images/4.png';
import five from '../images/5.png';
import six from '../images/6.png';
import seven from '../images/7.png';
import eight from '../images/8.png';
import nine from '../images/9.png';
import ten from '../images/10.png';
import eleven from '../images/11.png';
import twelve from '../images/12.png';
import thirteen from '../images/13.png';


export default function ProductsThree(){
	return(
		<Container fluid>
			<h1 className="mt-5 text-center">Moulded Rubber Fabrication</h1>
			{/*<p className="text-muted text-center">Imported Brands: Alfa Laval, APV, Schmidt, GEA, Tranter, Hisaka, etc.</p>*/}
			<Row className="d-flex justify-content-center">
				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					
				      <Image src={four} rounded fluid className="shadow card-border"/>
					
				</Col>

				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={five} rounded fluid className="shadow card-border"/>
				</Col>

				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={six} rounded fluid className="shadow card-border"/>
				</Col>


				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={seven} rounded fluid className="shadow card-border"/>
				</Col>


				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={eight} rounded fluid className="shadow card-border"/>
				</Col>

			</Row>

			<Row className="d-flex justify-content-center">
				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					
				      <Image src={nine} rounded fluid className="shadow card-border"/>
					
				</Col>

				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={ten} rounded fluid className="shadow card-border"/>
				</Col>

				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={eleven} rounded fluid className="shadow card-border"/>
				</Col>


				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={twelve} rounded fluid className="shadow card-border"/>
				</Col>


				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={thirteen} rounded fluid className="shadow card-border"/>
				</Col>

			</Row>

			<Row className="d-flex justify-content-center">
				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					
				      <Image src={nine} rounded fluid className="shadow card-border"/>
					
				</Col>

				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={ten} rounded fluid className="shadow card-border"/>
				</Col>

				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={eleven} rounded fluid className="shadow card-border"/>
				</Col>


				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={twelve} rounded fluid className="shadow card-border"/>
				</Col>


				<Col xs={12} s={6} md={6} lg={2} className="mt-2">
					<Image src={thirteen} rounded fluid className="shadow card-border"/>
				</Col>

			</Row>
			
		</Container>

		)
}