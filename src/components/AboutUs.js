import { Jumbotron, Container } from 'react-bootstrap';


export default function Highlight(){
	return(
		<Container>
		<Jumbotron className=" rounded shadow bg-white mt-5 mb-5 d-inline-block  text-center">
			<h1>About Us</h1>

			<p className="px-5">
			  TFT Industrial Sales is a direct importer and fabricator of various kinds of industrial products since year 2006. It has always been our goal to provide customer satisfaction through high quality products and excellent service at very competent prices.
			</p>

			<p className="px-5">
				We are a licensed importer of different kinds of Plate Heat Exchanger plates and gaskets, industrial adhesive tapes, and packaging materials. We are also in the business of fabricating moulded rubber and engineering plastic products, caster wheels, compressions springs, etc. For a full list of products offered, click PRODUCTS.
				Our client profile ranges from small enterprises to large multinational companies, spanning different industries across different locations in the country. 
			</p>
			<p className="px-5">
				Contact us if you have any inquiries and we would gladly be of assistance to your needs. 
			</p>
		</Jumbotron>
		</Container>

		)
}