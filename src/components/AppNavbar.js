import { Navbar, Nav, NavDropdown, Container, Dropdown, SplitButton } from 'react-bootstrap';
import logo from '../images/tft.png';
import { Link, NavLink } from 'react-router-dom';


export default function AppNavbar(){
	return(		

		<Navbar sticky="top" bg="light" expand="lg" className="shadow nav-radius">
		  
		    <Navbar.Brand as={Link} to="/"><img src={logo} className="logo"/><span className="logo-text">TFT Industrial Sales</span></Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto mr-5 pr-5">
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link as={NavLink} to="/aboutUs">About Us</Nav.Link>
           <NavDropdown title="Products" id="basic-nav-dropdown">
           
          
              <NavDropdown.Item as={NavLink} to="/plate-heat-exchanger-plates-and-gaskets">Plate Heat Exchanger Plates</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/gaskets-and-packings">Gaskets and Packings</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/moulded-rubber-fabrication">Moulded Rubber Fabrication</NavDropdown.Item>

              <NavDropdown.Item as={NavLink} to="/engineering-plastic-fabrication">Engineering Plastic Fabrication</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/compression-springs">Compression Springs</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/caster-wheels">Caster Wheels</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/insulation-materials">Insulation Materials</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/industrial-adhesive-tapes">Industrial Adhesive Tapes</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/lab-equipment">Lab Equipment</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/industrial-thermometers-and-pressure-gauge">Industrial Thermometers <br />and Pressure Gauge</NavDropdown.Item>
          
                 
            </NavDropdown>
          <Nav.Link as={NavLink} to="/contactUs">Contact Us</Nav.Link>
        </Nav>
      </Navbar.Collapse>
		
      
		</Navbar>






		)
}