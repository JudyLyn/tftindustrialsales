import { Carousel, Jumbotron, Container, fluid} from 'react-bootstrap';
import slide1 from '../images/slides/1.png';
import slide2 from '../images/slides/2.png';
import slide3 from '../images/slides/3.png';
import last from '../images/slides/last.png';
import first from '../images/slides/first.png';
import bg1 from '../images/bg1.png';
import bg2 from '../images/bg2.png';

export default function Highlight(){
	return(
		<>
			
				<Carousel fade interval={3000} className="shadow rounded carousel-height">
					<Carousel.Item>
						<img
						className="d-block w-100"
						src={first}
						alt="First slide"
						/>
						
					</Carousel.Item>
					<Carousel.Item>
						<img
						className="d-block w-100"
						src={slide1}
						alt="First slide"
						/>
						
					</Carousel.Item>

					<Carousel.Item>
						<img
						className="d-block w-100"
						src={slide2}
						alt="First slide"
						/>
						
					</Carousel.Item>

					<Carousel.Item>
						<img
						className="d-block w-100"
						src={slide3}
						alt="First slide"
						/>
						
					</Carousel.Item>

					<Carousel.Item>
						<img
						className="d-block w-100 h-70"
						src={last}
						alt="First slide"
						/>
						
					</Carousel.Item>
				
				</Carousel>

		</>

		)
}