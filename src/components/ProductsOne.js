import { Card, Button, Row, Col, Container, fluid } from 'react-bootstrap';
import one from '../images/1.png';
import two from '../images/2.png';
import three from '../images/3.png';

export default function ProductsOne(){
	return(
		<Container fluid>
			<h1 className="mt-5 text-center">Plate Heat Exchanger Plates and Gaskets</h1>
			<p className="text-center text-muted">Imported Brands: Alfa Laval, APV, Schmidt, GEA, Tranter, Hisaka, etc.</p>
			<Row className="">
				<Col xs={12} md={4}>
					<Card border="light" className="shadow m-3 card-border">
					  <Card.Img variant="top" src={one} />
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card border="light" className="shadow m-3">
					  <Card.Img variant="top" src={two} />	  
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card border="light" className="shadow m-3">
					  <Card.Img variant="top" src={three} />	  
					</Card>
				</Col>

			</Row>
			
		</Container>

		)
}