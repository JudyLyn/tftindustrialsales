import { Card, Row, Col, Container, Jumbotron } from 'react-bootstrap';




export default function ContactUs() {
	return(

		<Container>
			<Row>
				<Col xs={12} s={12} md={12} lg={12}>
					<Jumbotron className="edge rounded shadow bg-white mt-0 mb-3 d-inline-block w-100 text-center">
						<h1>Our Address</h1>

						<p className="">
							30 Gladiola St., Roxas District, Quezon City, 1103
						</p>
					</Jumbotron>
				</Col>

			</Row>
			<Row>
				<Col xs={12} s={12} md={12} lg={6}>
					<Jumbotron className=" contact edge rounded shadow bg-white w-100 text-center m-0">
						<h1>Email Us</h1>

						<p className="">
						 tftindustrial@gmail.com

						</p>
						<p className="">
						
							tftindustrial06@yahoo.com
						</p>

					</Jumbotron>
				</Col>
				<Col xs={12} s={12} md={12} lg={6}>
					<Jumbotron className=" contact edge rounded shadow bg-white w-100 text-center">
						<h1>Call Us</h1>

						<p className="">
						  Telephone Nos.:<br />
						(632) 8709-9744(632) <br />
						8709-5476(632)<br />
						 8876-0163</p>
						<p className="">
						  Telefax:
						
						  <br />
						(632) 8876-0163
						 </p>
					</Jumbotron>
				</Col>
			</Row>
		</Container>

		)
}