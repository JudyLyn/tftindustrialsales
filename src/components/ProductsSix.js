import { Card, Button, Row, Col, Image, Container, fluid } from 'react-bootstrap';
import four from '../images/4.png';
import five from '../images/5.png';
import six from '../images/6.png';
import seven from '../images/7.png';
import eight from '../images/8.png';
import nine from '../images/9.png';
import ten from '../images/10.png';
import eleven from '../images/11.png';
import twelve from '../images/12.png';
import thirteen from '../images/13.png';


export default function ProductsSix(){
	return(
		<Container fluid>
			<h1 className="mt-5 text-center">Caster Wheels</h1>
			
			<Row className="">
				<Col xs={12} md={4}>
					<Card border="light" className="shadow m-3 card-border">
					  <Card.Img variant="top" src={four} />
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card border="light" className="shadow m-3">
					  <Card.Img variant="top" src={five} />	  
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card border="light" className="shadow m-3">
					  <Card.Img variant="top" src={six} />	  
					</Card>
				</Col>

			</Row>
			
		</Container>

		)
}