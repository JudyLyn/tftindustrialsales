import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@material-ui/core';
import 'react-multi-carousel/lib/styles.css';

ReactDOM.render(

    <App />
 ,
  document.getElementById('root')
);

