import ContactUs from '../components/ContactUs';
import Map from '../components/Map';


import { Row, Col, Container } from 'react-bootstrap';

export default function ContactUsPage(){
	return(
		<div className="container-contactus mt-2">
			<Row>
				<Col xs={12} s={12} md={12} lg={6}>
					< ContactUs />
				</Col>
				<Col xs={12} s={12} md={12} lg={6}>
					< Map />
				</Col>
			</Row>
		</div>


		)
}