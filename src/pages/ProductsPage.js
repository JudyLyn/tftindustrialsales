import ProductsOne from '../components/ProductsOne';
import ProductsTwo from '../components/ProductsTwo';
import ProductsThree from '../components/ProductsThree';
import ProductsFour from '../components/ProductsFour';
import ProductsFive from '../components/ProductsFive';
import ProductsSix from '../components/ProductsSix';
import ProductsSeven from '../components/ProductsSeven';
import ProductsEight from '../components/ProductsEight';
import ProductsNine from '../components/ProductsNine';
import ProductsTen from '../components/ProductsTen';


export default function ProductsPage(){
	return(
		<>
			< ProductsOne />
			< ProductsTwo />
			< ProductsThree />
			< ProductsFour />
			< ProductsFive />
			< ProductsSix />
			< ProductsSeven />
			< ProductsEight />
			< ProductsNine />
			< ProductsTen />
		</>

		)
}