import AboutUs from '../components/AboutUs';
import bg from '../images/bg.png';
import Footer from '../components/Footer';
import { Row, Col, Container, fluid } from 'react-bootstrap';

export default function AboutUsPage(){
	return(
		
			<Container fluid>
				<Row>
					<Col sm={12} md={12} lg={12}>
					
						< AboutUs />
					</Col>
				</Row>
				
			</Container>
		
		)
}